import { font } from '../lib/design'

export default function buildTheme(props = {}) {
  return {
    textFont: props.customFontsLoaded ? 'FiraSans' : 'System',
    textBig: 24,
    textMedium: 18,
    textSmall: 12,

    fgColor:   '#fefefe',
    bgColor:   '#22C4B0',

    fgColor2:  '#333333',
    bgColor2:  '#FBB741',

    fgColor3:  '#38D473',
    bgColor3:  '#5A98E0',

    fgColor4:  '#fefefe',
    bgColor4:  '#9C6DE0',

    fgColor5:  '#fefefe',
    bgColor5:  '#38D473',

    bgColorDark:  '#3e3e3e',
    bgColorLight:  '#fefefe',

    muteColor: '#bebebe',
  }
}
