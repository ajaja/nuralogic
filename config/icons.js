module.exports = {
  gear: ['gear', 'EvilIcons'],
  bell: ['bell', 'EvilIcons'],
  up: ['chevron-up', 'EvilIcons'],
  down: ['chevron-down', 'EvilIcons'],
  left: ['chevron-left', 'EvilIcons'],
  right: ['chevron-right', 'EvilIcons'],
  arrowUp: ['arrow-up', 'EvilIcons'],
  arrowDown: ['arrow-down', 'EvilIcons'],
  arrowLeft: ['arrow-left', 'EvilIcons'],
  arrowRight: ['arrow-right', 'EvilIcons'],
  calendar: ['calendar', 'EvilIcons']
}
