import React, { Component } from 'react'
import {
  ListView,
  View,
  Text,
  Icon,
  Flex,
  Space,
  Underline,
} from '../components'
import SensorSummary from './SensorSummary'

class HomeScreen extends Component {
  static navigationOptions = {
    header: ({navigate}) => ({
      right: (
        <Icon name="gear" onPress={props=>navigate('Setting')} />
      ),
    }),
    title: 'Nuralogic',
  }

  constructor(props) {
    super(props)

    const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    const data = [
      {
        title: 'Dissolved Oxygen',
        unit: 'mg/L',
        value: 58.9,
        chartData: [Array(24).fill().map((v,i)=>{ return {x: i, y: parseInt(Math.random()*100)} })],
        bgColor: true,
      },
      {
        title: 'pH Sensors',
        unit: 'pH',
        value: 7.5,
        chartData: [Array(24).fill().map((v,i)=>{ return {x: i, y: parseInt(Math.random()*10)} })],
        bgColor2: true,
      },
      {
        title: 'Temperature',
        unit: '°C',
        value: 27,
        chartData: [Array(24).fill().map((v,i)=>{ return {x: i, y: parseInt(Math.random()*100)} })],
        bgColor3: true,
      },
      {
        title: 'Electrical Conductivity',
        unit: 'µS/cm',
        value: 489,
        chartData: [Array(24).fill().map((v,i)=>{ return {x: i, y: parseInt(Math.random()*100)} })],
        bgColor4: true,
      }
    ]
    this.state = {
      dataSource: ds.cloneWithRows(data)
    };
  }

  renderRow(data) {
    return <SensorSummary {...data} />
  }

  renderHeader() {
    return <Flex column justify="center" align="center" bgColorDark>
      <Space large />
      <Text small fgColor3>STABILITY SCORE</Text>
      <Text largest fgColor1>7.2</Text>
      <Space />
      <Underline style={{ width: 128, height: 2 }}/>
    </Flex>
  }

  render() {

    return (
      <ListView
        dataSource={this.state.dataSource}
        renderRow={this.renderRow}
        renderHeader={this.renderHeader}
      />
    );
  }
}

export default HomeScreen
