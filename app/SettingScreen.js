import React, { Component } from 'react'

import {
  View,
  ScrollView,
  Switch,
  Flex,
  List,
  Space,
} from '../components'

const { Header, Item, Text } = List

class SettingScreen extends Component {
  static navigationOptions = {
    title: 'Settings',
  }

  render() {
    return (
      <List>
        <Space small />

        <Header>Dissolved Oxygen Sensors</Header>
        <Item underline>
          <Text>Sensor #1</Text>
          <Switch />
        </Item>
        <Item>
          <Text>Sensor #2</Text>
          <Switch />
        </Item>

        <Space />

        <Header>pH Sensors</Header>
        <Item underline>
          <Text>Sensor #1</Text>
          <Switch />
        </Item>
        <Item underline>
          <Text>Sensor #2</Text>
          <Switch />
        </Item>
        <Item underline>
          <Text>Sensor #3</Text>
          <Switch />
        </Item>
        <Item>
          <Text>Sensor #4</Text>
          <Switch />
        </Item>
        
        <Space />

        <Header>Temperature Sensors</Header>
        <Item underline>
          <Text>Sensor #1</Text>
          <Switch />
        </Item>
        <Item>
          <Text>Sensor #2</Text>
          <Switch />
        </Item>

        <Space />

        <Header>Pumps</Header>
        <Item underline>
          <Text>Pump #1</Text>
          <Switch />
        </Item>
        <Item underline>
          <Text>Pump #2</Text>
          <Switch />
        </Item>
        <Item underline>
          <Text>Pump #3</Text>
          <Switch />
        </Item>
        <Item>
          <Text>Pump #4</Text>
          <Switch />
        </Item>

        <Space />
      </List>
    )
  }
}


export default SettingScreen
