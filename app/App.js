import React, { Component } from 'react'
import {
  StackNavigator,
} from '../components'

import HomeScreen from './HomeScreen'
import SettingScreen from './SettingScreen'

export default StackNavigator({
  Home: { screen: HomeScreen },
  Setting: { screen: SettingScreen },
})
