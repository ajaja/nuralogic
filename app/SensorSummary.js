import React, { Component } from 'react'
import {
  WingBlank,
  Flex,
  Text,
  LineChart,
  Space,
  Underline,
} from '../components'

class SensorSummary extends Component {
  render() {
    const { title, value, unit, chartData } = this.props

    return (
      <Flex column {...this.props}>
        <Space />
        <WingBlank>
          <Flex row align="center">
            <Text weight={500}>{title}</Text>
            <Text extraLarge weight={200}>
              {value} <Text small weight={100}>{unit}</Text>
            </Text>
          </Flex>
        </WingBlank>
        <Space />

        <LineChart
          height={73}
          data={chartData}
          numOfTicks={3}
          xLabel={v => v === 12 ? 'noon' : v}
        />
        <Space small />
      </Flex>
    )
  }
}

export default SensorSummary
