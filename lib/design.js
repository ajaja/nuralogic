import React, { Component } from 'react'
import { ThemeProvider } from 'styled-components'
import buildTheme from '../config/theme'

export function connectTheme(WrappedComponent) {
  return class extends Component {
    render() {
      return <ThemeProvider theme={buildTheme(this.props)}>
        <WrappedComponent {...this.props} />
      </ThemeProvider>
    }
  }
}
