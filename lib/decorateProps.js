import React, { Component } from 'react';

const getDisplayName = (WrappedComponent) => (
  WrappedComponent.displayName || WrappedComponent.name || 'Component'
);

export default (EmbeddedComponent, props) => (class extends Component {
  static displayName = `Decorated(${getDisplayName(EmbeddedComponent)})`;

  render() {
    Object.keys(props).forEach(k => {
      if (typeof props[k] === 'function') {
        props[k] = props[k](this.props)
      }
    })

    return (
      <EmbeddedComponent { ...this.props } { ...props } />
    );
  }
});
