import React, { Component } from 'react'
import { View, ScrollView } from 'react-native'
import { Text as StyledText, WingBlank, Space, Flex, Underline } from './styled'

class List extends Component {
  render() {
    return <ScrollView>
      {this.props.children}
    </ScrollView>
  }
}

class Header extends Component {
  render() {
    return <WingBlank>
      <Space small />
      <StyledText fgColor2 small weight={500}>
        {this.props.children.toUpperCase()}
      </StyledText>
      <Space small />
    </WingBlank>
  }
}

class Item extends Component {
  render() {
    return <WingBlank bgColorLight>
      <View style={{ borderColor: '#d3dad7', borderBottomWidth: this.props.underline ? 1 : 0 }}>
        <Space />
        <Flex row align="center">
          {this.props.children}
        </Flex>
        <Space />
      </View>
    </WingBlank>
  }
}

class Text extends Component {
  render() {
    return <StyledText fgColor2>{this.props.children}</StyledText>
  }
}

List.Header = Header
List.Item = Item
List.Text = Text

export default List
