import React, { Component } from 'react'
import Svg, {
  Text,
} from 'react-native-svg';
import { View } from 'react-native'
import { line, curveCatmullRom } from 'd3-shape'
import { scaleLinear, scaleTime } from 'd3-scale'
import { SvgPath, Flex } from './styled'
import Axis from './Axis'

class LineChart extends Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  updateContainerSizes(e) {
    const layout = e.nativeEvent.layout
    this.setState({
      containerWidth: layout.width,
      containerHeight: layout.height || 200,
    })
  }

  getSizes() {
    let { width, height } = this.props

    width = width || this.state.containerWidth
    height = height || this.state.containerHeight

    return { width, height }
  }

  render() {
    let { data } = this.props
    let { width, height } = this.getSizes()

    if (width && height) {
      var { numOfTicks } = this.props
      var axisHeight = 30
      var chartHeight = height - axisHeight
      var paths = svgPaths(data, width, chartHeight)
      var { xScale, yScale } = paths[0]
    }

    return (
      <View
        onLayout={this.updateContainerSizes.bind(this)}>
        {
          width && height ? (
            <Svg width={width} height={height}>
              {
                paths.map((p, i) => <SvgPath d={p.paths} key={i} />)
              }
              {/* <Axis
                height={axisHeight}
                viewportWidth={width}
                viewportHeight={height}
                xScale={xScale}
                numOfTicks={numOfTicks}
                xLabel={this.props.xLabel}
              /> */}
            </Svg>
          ) : null
        }
      </View>
    )
  }
}

export default LineChart


const colors = [ '#000000', '#F05050', ]
const createScales = (dataPoints, width, height) => {
  const xScale = scaleLinear().domain([0, width])
  const yScale = scaleLinear().domain([height, 0])

  const xValues = dataPoints.map(pair => pair.x)
  const yValues = dataPoints.map(pair => pair.y)

  xScale.range([Math.min(...xValues), Math.max(...xValues)])
  yScale.range([Math.min(...yValues)*1.2, Math.max(...yValues)*1.2])

  return {xScale, yScale}
}
const svgPaths = (data, width, height) => {
  return data.map((d, i) => {
    const { xScale, yScale } = createScales(d, width, height)
    const paths = line()
      .curve(curveCatmullRom.alpha(0.5))
      .x(d => xScale.invert(d.x))
      .y(d => yScale.invert(d.y))(d)

      console.log(paths)

    return { xScale, yScale, paths }
  })
}
