import React, { Component } from 'react'
import configIcons from '../config/icons'
import { brandIcons } from '../rn-wrapper'


class Icon extends Component {
  render() {
    const { name } = this.props
    const [actualName, brand] = configIcons[name]
    return React.createElement(brandIcons[brand], {size: 32, ...this.props})
  }
}

export default Icon
