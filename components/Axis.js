import React, { Component } from 'react'
import { View, SvgText } from './styled'
import { G } from 'react-native-svg';

class Axis extends Component {
  render() {
    const { xScale, fontSize, stroke, numOfTicks,
      xLabel, viewportHeight, viewportWidth, height } = this.props
    const gap = viewportWidth/(numOfTicks - 1)

    return (
      <G>
        {
          Array(numOfTicks).fill().map((_, i) => {
            const label = Math.round(xScale(i*gap))
            return <SvgText
              key={i}
              fontSize={fontSize || 20}
              stroke={stroke || '#aaaaaa'}
              strokeWidth={0.2}
              textAnchor={i === 0 ? 'start' : (i < numOfTicks - 1 ? 'middle' : 'end')}
              x={i * gap}
              y={viewportHeight - (height - 10)}>
              { xLabel ? xLabel(label) : label }
            </SvgText>
          })
        }
      </G>
    )
  }
}

export default Axis
