import styled from 'styled-components/native'
import { Text as SvgText, Path as SvgPath } from 'react-native-svg'
import decorateProps from '../../lib/decorateProps'
import buildTheme from '../../config/theme'

const theme = buildTheme({})

module.exports = {
  Flex: styled.View`
    flex: 1;
    flex-direction: ${ props => trueKey(props, ['row', 'column']) || 'row' };
    align-items: ${ props => props.align || 'stretch' };
    justify-content: ${ props => props.justify || 'space-between' };
    background-color: ${bgColorVal()};
  `,
  WingBlank: styled.View`
    padding-left: ${gutterVal(20)};
    padding-right: ${gutterVal(20)};
    background-color: ${bgColorVal()};
    flex: 1;
  `,
  Space: styled.View`
    height: ${gutterVal(20)};
    width: ${gutterVal(20)};
  `,
  View: styled.View`
    background-color: ${bgColorVal()};
  `,
  Underline: styled.View`
    width: 600;
    background-color: ${fgColorVal()};
    height: 1;
  `,
  Text: styled.Text`
    font-family: ${fontFamilyVal()};
    font-weight: ${props => props.weight || 'normal'};
    font-size: ${fontSizeVal(16)};
    color: ${fgColorVal()};
  `,
  SvgText: decorateProps(SvgText, {
    fontSize: fontSizeVal(11),
  }),
  SvgPath: decorateProps(SvgPath, {
    stroke: fgColorVal(),
    strokeWidth: 1,
    fill: 'none',
  })
}

function trueKey (props, keys) {
  return Object.keys(props).filter(p=>keys.includes(p) && props[p] === true)[0]
}

function trueVal (props, keyVal) {
  return keyVal[trueKey(props, Object.keys(keyVal))]
}

function gutterVal (defaultVal) {
  return props => trueVal(props, {
    extraSmall: 5, small: 10, medium: 20, large: 30, extraLarge: 40
  }) || defaultVal
}

function fontSizeVal (defaultVal) {
  return props => trueVal(props, {
    extraSmall: 10, small: 13, medium: 16, large: 20, extraLarge: 30, largest: 54
  }) || defaultVal
}

function fgColorVal (defaultColor) {
  return props => {
    const { fgColor, fgColor2, fgColor3, fgColor4, fgColor5 } = theme
    return trueVal(props, { fgColor, fgColor2, fgColor3, fgColor4, fgColor5 }) || defaultColor || theme.fgColor
  }
}

function bgColorVal (defaultColor) {
  return props => {
    const { bgColor, bgColor2, bgColor3, bgColor4, bgColor5, bgColor6, bgColorDark, bgColorLight } = theme
    return trueVal(props, { bgColor, bgColor2, bgColor3, bgColor4, bgColor5, bgColorDark, bgColorLight }) || defaultColor || 'transparent'
  }
}

function fontFamilyVal (defaultVal) {
  return props => {
    const fontName = defaultVal || props.theme.textFont || 'System'
    if (fontName == 'System') {
      return 'System'
    } else {
      const weight = props.weight || 400
      switch (weight)
      {
        case 100:
          return fontName + '-ExtraLight'
        case 200:
          return fontName + '-Light'
        case 300:
          return fontName + '-Thin'
        case 400:
          return fontName + '-Regular'
        case 500:
          return fontName + '-Medium'
        case 600:
          return fontName + '-Black'
        case 700:
          return fontName + '-Bold'
        case 800:
          return fontName + '-SemiBold'
        case 900:
          return fontName + '-ExtraBold'
          break;
        default:
          return fontName
      }
    }
  }
}
