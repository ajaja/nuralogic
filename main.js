import { registerComponent, connectFonts } from './rn-wrapper'

import { compose } from 'redux'
import { connectTheme } from './lib/design'
import App from './app/App'

const connectDefaults = compose(connectFonts, connectTheme)
const connectedApp = connectDefaults(App)

registerComponent('nuralogic', connectedApp)
