import React, { Component } from 'react'
import Exponent, { Font, Components } from 'exponent'
import { EvilIcons } from '@exponent/vector-icons'

export function connectFonts(WrappedComponent) {
  return class extends Component {
    state = {
      fontsAreLoaded: false,
    }

    async componentWillMount() {
      await Font.loadAsync(require('../../config/fonts'))
      this.setState({fontsAreLoaded: true})
    }

    render() {
      if (!this.state.fontsAreLoaded) {
        return <Components.AppLoading />
      }

      return <WrappedComponent
        {...this.props}
        customFontsLoaded={this.state.fontsAreLoaded}
      />
    }
  }
}

export function registerComponent(name, App) {
  Exponent.registerRootComponent(App)
}

export const brandIcons = {
  EvilIcons
}
