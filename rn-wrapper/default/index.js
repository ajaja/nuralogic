import React, { Component } from 'react'
import EvilIcons from 'react-native-vector-icons/EvilIcons'
import { AppRegistry } from 'react-native'

export function connectFonts(WrappedComponent) {
  return class extends Component {
    render() {
      return <WrappedComponent {...this.props} />;
    }
  }
}

export function registerComponent(name, App) {
  AppRegistry.registerComponent(name, () => App)
}

export const brandIcons = {
  EvilIcons
}
